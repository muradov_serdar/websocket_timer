var server = require('ws').Server;
var s = new server({ port:1001 });

var runningUser = false;
var willRunUser = false;
var willRunUserTime = false;

s.on('connection', function (ws) {

   ws.on('message', function (message) {

       message = JSON.parse(message);

       if (message.type == "initialize"){
           ws.username = message.username;
           ws.time = message.time;
           ws.userId = message.userId;

           serdToUsers('new_user');
       }
       if (message.type == 'start_timer'){
           if (!runningUser){
               runningUser = message.id;
               serdToUsers('start_timer', message.id, message.time);
           } else {
               willRunUser = message.id;
               willRunUserTime = message.time;
               serdToUsers('stop_and_get_time', runningUser);
           }
       }
       if (message.type == 'stop_timer'){
           ws.time = message.time;
           runningUser = false;
           willRunUser = false;
           willRunUserTime = false;
           serdToUsers('stop_timer', message.id, message.time);
       }
       if (message.type == 'stop_and_send_time'){
           ws.time = message.time;
           serdToUsers('stop_timer', message.id, message.time);
           runningUser = willRunUser;
           serdToUsers('start_timer', willRunUser, willRunUserTime);
           willRunUser = false;
           willRunUserTime = false;
       }

       console.log(runningUser, willRunUser, willRunUserTime);

   });

    function serdToUsers(type, id = 0, time = 0) {
        var clients = [];
        s.clients.forEach(function e(client) {
            var user = {
                username: client.username,
                id: client.userId,
                time: client.time,
            };
            clients.push(user);
        });

        if (type == 'new_user'){
            s.clients.forEach(function e(client) {
                client.send(JSON.stringify({
                    type: type,
                    clients: clients,
                }));
            });
        } else
        if (type == 'start_timer'){
            s.clients.forEach(function e(client) {
                client.send(JSON.stringify({
                    type: type,
                    clients: clients,
                    id: id,
                    time: time,
                }));
            });
        } else
        if (type == 'stop_timer'){
            s.clients.forEach(function e(client) {
                client.send(JSON.stringify({
                    type: type,
                    clients: clients
                }));
            });
        } else
        if (type == 'stop_and_get_time'){
            s.clients.forEach(function e(client) {
                client.send(JSON.stringify({
                    type: type,
                    clients: clients,
                    id: id,
                    time: time,
                }));
            });
        }
    }

   ws.on('close', function () {
       runningUser = false;
       willRunUser = false;
       willRunUserTime = false;
       serdToUsers('stop_timer');
   });
});